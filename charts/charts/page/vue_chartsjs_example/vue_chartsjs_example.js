frappe.pages["vue_chartsjs_example"].on_page_load = function (wrapper) {
	frappe.ui.make_app_page({
		parent: wrapper,
		title: __("vue_chartsjs_example"),
		single_column: true,
	});
};

frappe.pages["vue_chartsjs_example"].on_page_show = function (wrapper) {
	load_desk_page(wrapper);
};

function load_desk_page(wrapper) {
	let $parent = $(wrapper).find(".layout-main-section");
	$parent.empty();

	frappe.require("vue_chartsjs_example.bundle.js").then(() => {
		frappe.vue_chartsjs_example = new frappe.ui.Vue_Chartsjs_Example({
			wrapper: $parent,
			page: wrapper.page,
		});
	});
}